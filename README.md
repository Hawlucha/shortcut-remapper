# shortcut-remapper

An application (likely will be in Electron\*) that remaps operating system and installed applications' keyboard shortcuts based on user's preferred keyboard layout.

\*Did your anti-Electron fight-or-flight response activate? [Read this blog post of a developer who was so tired of the Electron-versus-Native war of elitism he had to write about it](https://asylum.madhouse-project.org/blog/2018/10/26/Walking-in-my-shoes/) ([Archived](https://web.archive.org/web/20201108115719/https://asylum.madhouse-project.org/blog/2018/10/26/Walking-in-my-shoes/)). After that, you can either help me out with nativeïfying this application since you care enough to even groan about me choosing to use Electron, you can do away with contributing to this likely-to-be-Electron application despite your dislike, or you can shut the fuck up.
